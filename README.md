# cs-5-strata

A repo containing a python 3.6 flask web app for tracking strata issues. <br> 
This was created as a requirement of the Code.Sydney March 2019 Flask Course. <br>
<a href = "https://docs.google.com/document/d/1w42q1kLZlV4S_EJHAWoUlbHZfupS0GvXCqXlHKpj6ms/edit">Link to the project brief</a>