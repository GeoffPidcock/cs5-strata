create database if not exists strata;
use strata;
create table if not exists issues(
	issue_number INT(11) auto_increment,
    issue_description VARCHAR(45),
    Primary Key (issue_number)
) Engine = InnoDB;
/* seed data */
insert into issues(issue_description)
values 
	('Lift is not working')
    , ('Unit 1 requires a new garbage bin')
    , ('Graffiti found on retaining walls');