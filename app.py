# Import libraries
from flask import Flask, request, url_for, redirect, render_template, g
from flaskext.mysql import MySQL

app = Flask(__name__)

# Config app to run in debug mode
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'Thisisasecret!'

# Config app to connect to MySQL DB
app.config['MYSQL_DATABASE_USER'] = 'GeoffPidcock'
app.config['MYSQL_DATABASE_PASSWORD'] = 'adminadmin'
app.config['MYSQL_DATABASE_DB'] = 'strata'
app.config['MYSQL_DATABASE_HOST'] = 'GeoffPidcock.mysql.pythonanywhere-services.com'

# Initialize MySQL
flaskmysql = MySQL()
flaskmysql.init_app(app)

# Define helper functions
def connect_db(flaskmysql):
    con = flaskmysql.connect()
    return con

def get_db(flaskmysql):
    if not hasattr(g, 'mysql_db'):
        g.mysql_db = connect_db(flaskmysql)
    return g.mysql_db

def Convert(tup, di):
    # Takes a tuple and a dict, and populates the dict with values from the tuple.
    di = dict(tup)
    return di

# Close DB connection after transaction
@app.teardown_appcontext
def close_db(error):
    if hasattr(g, 'mysql_db'):
        g.mysql_db.close()

# Render Homepage
@app.route('/')
# Define a function that reads a database and passes the result to a html template
def index():
    con = get_db(flaskmysql)
    cur = con.cursor()
    cur.execute('select issue_number, issue_description from issues')
    results = cur.fetchall()
    dictionary = dict()
    results_dict = Convert(results,dictionary)
    # ToDo - generate an edit URL for each issue in the dict
    # ToDo - link create page to home page
    return render_template('home.html',results_dict=results_dict)

# Render create page
@app.route('/create', methods=['GET', 'POST'])
def create():
    if request.method == 'GET':
        return render_template('create.html')
    else:
        description = request.form['Description']
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('insert into issues (issue_description) values (%s)', (description))
        con.commit()
        return redirect(url_for('index'))

@app.route('/edit', methods=['POST', 'GET'], defaults={'key' : '1'})
@app.route('/edit/<string:key>', methods=['GET', 'POST'])
def edit(key):
    if request.method == 'GET':
        con = get_db(flaskmysql)
        cur = con.cursor()
        cur.execute('select issue_number, issue_description from issues where issue_number = (%s)', (key))
        results = cur.fetchall()
        return render_template('edit.html',results=results)
    else:
        if request.form['Type'] == 'Update':
            description = request.form['Description']
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('update issues \
                        set issue_description = (%s) \
                        where issue_number = (%s);',
                        (description, key))
            con.commit()
        elif request.form['Type'] == 'Delete':
            con = get_db(flaskmysql)
            cur = con.cursor()
            cur.execute('delete from issues where issue_number = (%s);',(key))
            con.commit()
        return redirect(url_for('index'))

if __name__ == '__main__':
    app.run()